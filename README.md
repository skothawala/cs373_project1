# FacilityProject

- Simple project that implements an apartment complex object design using Intermediate OOP principles such as Inverted Dependency Injection.

# Authors
- Katie Eaton
- Saad Kothawala
- Ryan Sevilla