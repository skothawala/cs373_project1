package com.rskesk.FacilityProject.client;

import com.rskesk.FacilityProject.model.Facility.ApartmentBuilding;
import com.rskesk.FacilityProject.model.Facility.ApartmentUnit;
import com.rskesk.FacilityProject.model.Facility.Facility;
import com.rskesk.FacilityProject.model.Facility.FacilityInformation;
import com.rskesk.FacilityProject.model.Facility.FacilityInformationHandler;
import com.rskesk.FacilityProject.model.Facility.User.FacilityUser;

public class ExampleClient {

	public static void main(String[] args) throws Exception {
		
		/***************SETUP***************/
		
		FacilityInformation apartmentfI = new FacilityInformationHandler();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		apartmentfI.setFacilityCity("Chicago");
		apartmentfI.setFacilityName("LakeSide Apartment Complex");
		apartmentfI.setFacilityState("IL");
		apartmentfI.setFacilityZip("60659");

		ApartmentBuilding lakeSideApartmentComplexes = new ApartmentBuilding(apartmentfI);
		
		ApartmentUnit unit1 = new ApartmentUnit();
		unit1.getFacilityUses().assignFacilityToUse(new FacilityUser("Saad", "Kothawala", "..."), "Living");
		unit1.getFacilityUses().vacateFacility();
		
		unit1.getFacilityUses().assignFacilityToUse(new FacilityUser("Katie", "Eaton", "..."), "investment");
		unit1.getFacilityUses().vacateFacility();
		
		unit1.getFacilityUses().assignFacilityToUse(new FacilityUser("Saad", "Kothawala", "..."), "Living");
		unit1.setApartmentName("Saad's Apt");
		lakeSideApartmentComplexes.addNewFacility(unit1);
		
		ApartmentUnit unit2 = new ApartmentUnit();
		unit2.getFacilityUses().assignFacilityToUse(new FacilityUser("Katie", "Eaton", "..."), "investment");
		unit2.setApartmentName("Katie's Apt");
		lakeSideApartmentComplexes.addNewFacility(unit2);
		
		ApartmentUnit unit3 = new ApartmentUnit();
		unit3.getFacilityUses().assignFacilityToUse(new FacilityUser("Ryan", "Sevilla", "..."), "partying");
		unit3.setApartmentName("Ryan's Apt");
		lakeSideApartmentComplexes.addNewFacility(unit3);
		
		ApartmentUnit pentHouse = new ApartmentUnit();
		pentHouse.setApartmentName("PentHouse");
		lakeSideApartmentComplexes.addNewFacility(pentHouse);
		
		
		
		unit2.getFacilityMaintainence().makeFacilityMaintRequest("Shower not working", unit2.getFacilityUses().getCurrentFacilityUse());
		unit2.getFacilityMaintainence().acceptMaintenance(true);
		unit2.getFacilityMaintainence().scheduleMaintenance();
		unit2.getFacilityMaintainence().completeMaintainence();
		
		unit3.getFacilityMaintainence().makeFacilityMaintRequest("Faucet not working", unit3.getFacilityUses().getCurrentFacilityUse());
		unit3.getFacilityMaintainence().acceptMaintenance(true);
		
		unit3.getFacilityMaintainence().makeFacilityMaintRequest("Door not closing", unit3.getFacilityUses().getCurrentFacilityUse());
		unit3.getFacilityMaintainence().scheduleMaintenance();
		
		
		
		/***************END SETUP***************/
		
		
		System.out.println("Facility Name:\t\t" + lakeSideApartmentComplexes.getFacilityInformation().getFacilityName());
		System.out.println("Apartment Address:\t" + lakeSideApartmentComplexes.getFacilityInformation().getFacilityStreetAddress());
		System.out.println("\t\t\t" + lakeSideApartmentComplexes.getFacilityInformation().getFacilityCity() + ", " 
					+ lakeSideApartmentComplexes.getFacilityInformation().getFacilityState() + " "
					+ lakeSideApartmentComplexes.getFacilityInformation().getFacilityZip());
		
		System.out.println("Number of Units: " + lakeSideApartmentComplexes.getApartmentUnits().size());
		System.out.println("Facility Use Rate: " + lakeSideApartmentComplexes.calcUsageRate(true) + "%");
		
		
		System.out.println("List of Apartments:");
		for(Facility apartment : lakeSideApartmentComplexes.getApartmentUnits()) {
			System.out.println("*********************************************");
			System.out.println("\t\tName: " + apartment.getFacilityInformation().getFacilityName());
			
			if(apartment.getFacilityUses().isFacilityInUse()) {
				System.out.println("\t\tFacility is used for: " + apartment.getFacilityUses().getCurrentFacilityUse().getUseDescription());
				System.out.println("\t\tFacility started being used: " + apartment.getFacilityUses().getCurrentFacilityUse().getStartUseDate());
				System.out.println("\t\tFacility is being used by: " + apartment.getFacilityUses().getCurrentFacilityUse().getFacilityUser());
				System.out.println("\t\tFacility Turnovers: " + apartment.calcTurnOver());
			} else {
				System.out.println("\t\tFacility Not In Use");
			}
					
			System.out.println("*********************************************");
		}
		
		System.out.println("\nFacility Problems:");
		lakeSideApartmentComplexes.getFacilityMaintainence().listFacilityProblems();
		System.out.println("\nFacility Cost\t" + lakeSideApartmentComplexes.getFacilityMaintainence().calcMaintenanceCostForFacility());
		

		System.out.println("\n\n\nMischief managed");
	}

}
