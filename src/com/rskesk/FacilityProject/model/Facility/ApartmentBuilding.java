package com.rskesk.FacilityProject.model.Facility;

import java.util.ArrayList;

import com.rskesk.FacilityProject.model.Facility.Inspection.Inspectable;
import com.rskesk.FacilityProject.model.Facility.User.FacilityUser;
import com.rskesk.FacilityProject.model.Facility.User.Usable;
import com.rskesk.FacilityProject.model.Facility.User.UsableHandler;
import com.rskesk.FacilityProject.model.Maintenance.Maintainable;
import com.rskesk.FacilityProject.model.Maintenance.MaintenanceHandler;

public class ApartmentBuilding extends Facility {

	private ArrayList<Facility> apartmentUnits;
	
	public ApartmentBuilding(FacilityInformation fI) throws Exception {
		super();
		this.facilityInformation = fI;
		this.apartmentUnits = new ArrayList<Facility>();
		this.facilityUses = new UsableHandler();
		this.facilityMaintainence = new MaintenanceHandler();
		
		this.getFacilityInformation().setFacilityType("APARTMENT_COMLPEX");
		this.getFacilityUses().assignFacilityToUse(new FacilityUser("", "", "000"), "");
	}
	
	@Override
	public FacilityInformation getFacilityInformation() {
		return this.facilityInformation;
	}

	@Override
	public void addNewFacility(Facility f) throws Exception {
		if(!(f instanceof ApartmentUnit))
			throw new Exception("ApartmentBuilding can only add new ApartmentUnit in addNewFacility");
		
		f.getFacilityInformation().setFacilityCity(this.getFacilityInformation().getFacilityCity());
		f.getFacilityInformation().setFacilityState(this.getFacilityInformation().getFacilityState());
		f.getFacilityInformation().setFacilityStreetAddress(this.getFacilityInformation().getFacilityStreetAddress());
		f.getFacilityInformation().setFacilityZip(this.getFacilityInformation().getFacilityZip());
		((ApartmentUnit) f).setFacilityMaintainence(this.getFacilityMaintainence());
		this.apartmentUnits.add(f);		
	}
	
	public ArrayList<Facility> getApartmentUnits() {
		return this.apartmentUnits;
	}

	/**
	 * @return boolean ? facilityToRemove Found or not
	 */
	@Override
	public boolean removeFacility(String facilityName) throws Exception {
		for(Facility apartmentUnit : this.getApartmentUnits()) {
			if(apartmentUnit.getFacilityInformation().getFacilityName().equals(facilityName)){
				this.getApartmentUnits().remove(apartmentUnit);
				return true;
			}
		}
		return false;
	}

	@Override
	public void listFacilities() {
		for(Facility apartmentUnit : this.getApartmentUnits()) { 
			System.out.println("************************************");
			System.out.println("Name: " + apartmentUnit.getFacilityInformation().getFacilityName());
			System.out.println("************************************");
		}
	}

	@Override
	public Inspectable getFacilityInspection() {
		return this.facilityInspection;
	}

	@Override
	public void setFacilityInspection(Inspectable fI) {
		this.facilityInspection = fI;		
	}

	@Override
	public Usable getFacilityUses() {
		return this.facilityUses;
	}

	@Override
	public void setFacilityUses(Usable u) {
		this.facilityUses = u;		
	}

	@Override
	public double calcUsageRate(boolean asPercent) {
		double total = 0;
		for(Facility unit : this.getApartmentUnits()) {
			total += unit.calcUsageRate(false);
		}
		
		return asPercent ? total / this.apartmentUnits.size() * 100 : total / this.apartmentUnits.size() ;
	}

	@Override
	public double calcTurnOver() {
		return this.facilityUses.getIndividualFacilityUses().size();
	}

	@Override
	public int requestAvailableCapacity() {
		int total = 0;
		for(Facility unit : this.getApartmentUnits()) {
			total += unit.requestAvailableCapacity();
		}
		
		return total;
	}

	@Override
	public Maintainable getFacilityMaintainence() {
		return this.facilityMaintainence;
	}


}
