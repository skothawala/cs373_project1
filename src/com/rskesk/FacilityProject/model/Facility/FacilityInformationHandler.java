package com.rskesk.FacilityProject.model.Facility;

public class FacilityInformationHandler implements FacilityInformation{

	private String name;
	
	private String type;
	
	private String streetAddress;
	private String zip;
	private String state;
	private String city;
	
	

	@Override
	public void setFacilityName(String name) {
		this.name = name;		
	}

	
	@Override
	public void setFacilityStreetAddress(String fSA) {
		this.streetAddress = fSA;		
	}

	@Override
	public void setFacilityZip(String zip) {
		this.zip = zip;		
	}

	@Override
	public void setFacilityState(String state) {
		this.state = state;		
	}

	@Override
	public void setFacilityCity(String city) {
		this.city = city;		
	}

	@Override
	public String getFacilityStreetAddress() {
		return this.streetAddress;
	}

	@Override
	public String getFacilityZip() {
		return this.zip;
	}

	@Override
	public String getFacilityState() {
		return this.state;
	}

	@Override
	public String getFacilityCity() {
		return this.city;
	}


	@Override
	public String getFacilityName() {
		return this.name;
	}


	@Override
	public void setFacilityType(String type) {
		this.type = type;
	}


	@Override
	public String getFacilityType() {
		return this.type;
	}

}
