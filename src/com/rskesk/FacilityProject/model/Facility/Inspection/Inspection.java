package com.rskesk.FacilityProject.model.Facility.Inspection;

import java.time.LocalDate;

public class Inspection {
	private boolean passedInsp;
	private LocalDate dateOfInspection;
	
	public Inspection() {
		this.passedInsp = false;
		this.dateOfInspection = LocalDate.now();
	}
	
	public Inspection(boolean result, LocalDate time) {
		this.passedInsp = result;
		this.dateOfInspection = time;
	}
	
	public void setInspectionResult(boolean result) {
		this.passedInsp = result;
	}
	
	public LocalDate getDateOfInspection() {
		return this.dateOfInspection;
	}
	
	public String getPrettyPrint() {
		return "Date of Inspection: " + this.dateOfInspection.getMonth() + "/" + this.dateOfInspection.getDayOfMonth() + "/"
				+ this.dateOfInspection.getYear() + "\n" + "Result: " + this.passedInsp; 
	}

}
