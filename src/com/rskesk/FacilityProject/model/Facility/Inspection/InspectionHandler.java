package com.rskesk.FacilityProject.model.Facility.Inspection;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by keetz on 2/9/17.
 */
public class InspectionHandler implements Inspectable {
    
	private int frequencyOfInspectionInDays;
    private ArrayList<Inspection> inspectionObjects;
    
    public InspectionHandler(){
    	this.frequencyOfInspectionInDays = 60;
    	this.inspectionObjects = new ArrayList<Inspection>();
    }
    
    public InspectionHandler(int frequency){
    	this.frequencyOfInspectionInDays = frequency;
    	this.inspectionObjects = new ArrayList<Inspection>();
    }
    
    public void setFrequnecy(int frequencyOfInsp){
        this.frequencyOfInspectionInDays=frequencyOfInsp;
    }
    
    public int getFrequency(){
        return frequencyOfInspectionInDays;
    }
    
    public ArrayList<Inspection> getInspections() {
    	return this.inspectionObjects;
    }
    
	@Override
	public void listInspections() {
		for(Inspection inspectionObject: this.getInspections()) {
			System.out.println("************************************");
			System.out.println(inspectionObject.getPrettyPrint());
			System.out.println("************************************");
		}
	}
	@Override
	public void scheduleInspection() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public LocalDate getNextInspectionDate() {
		if(this.getInspections().size() == 0)
			return LocalDate.now();
		
		return this.getInspections().get(this.getInspections().size() - 1).getDateOfInspection().plusDays(this.frequencyOfInspectionInDays);
	}


	@Override
	public void addInspection(Inspection inspection) {
		this.inspectionObjects.add(inspection);
	}

}
