package com.rskesk.FacilityProject.model.Facility.User;

/**
 * Created by keetz on 2/9/17.
 */
public class FacilityUser {
	private String firstName;
	private String lastName;
	private String ssn;

	public FacilityUser(String firstName, String lastName, String ssn) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
	}
	
	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}
	
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

}
