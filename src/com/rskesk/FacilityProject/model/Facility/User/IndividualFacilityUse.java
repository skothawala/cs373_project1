package com.rskesk.FacilityProject.model.Facility.User;

import java.time.LocalDate;

public class IndividualFacilityUse {
	
	private FacilityUser user;
	private LocalDate startUseDate;
	private LocalDate endUseDate;
	private String useDescription;
	
	public IndividualFacilityUse(FacilityUser u, String useDescription) {
		this.user = u;
		this.useDescription = useDescription;
		this.startUseDate = LocalDate.now();
		this.endUseDate = null;
		
	}

	public IndividualFacilityUse(FacilityUser u, String useDescription, LocalDate startDate) {
		this.user = u;
		this.useDescription = useDescription;
		this.startUseDate = startDate;
		this.endUseDate = null;
	}
	
	public IndividualFacilityUse(FacilityUser u, String useDescription, LocalDate startDate, LocalDate endDate) {
		this.user = u;
		this.useDescription = useDescription;
		this.startUseDate = startDate;
		this.endUseDate = endDate;
	}
	
	public void endUse() {
		this.endUseDate = LocalDate.now();
	}
	
	public String getUseDescription() {
		return this.useDescription;
	}
	
	public FacilityUser getFacilityUser() {
		return this.user;
	}
	
	public LocalDate getStartUseDate() {
		return this.startUseDate;
	}
	
	public LocalDate getEndUseDate() {
		return this.endUseDate;
	}
	
}
