package com.rskesk.FacilityProject.model.Facility.User;

import java.util.ArrayList;

public interface Usable {
	boolean isInUseDuringInterval();
	void assignFacilityToUse(FacilityUser fU, String useDescription) throws Exception;
	void vacateFacility();
	
	boolean isFacilityInUse();
	IndividualFacilityUse getCurrentFacilityUse() throws Exception;
	
	ArrayList<IndividualFacilityUse> getIndividualFacilityUses();
	void setFacilityUses(ArrayList<IndividualFacilityUse> individualFacilityUses);
	
	
	
}
