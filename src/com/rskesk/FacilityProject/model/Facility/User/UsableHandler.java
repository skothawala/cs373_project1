package com.rskesk.FacilityProject.model.Facility.User;

import java.util.ArrayList;

public class UsableHandler implements Usable{

	private ArrayList<IndividualFacilityUse> individualFacilityUses;
	
	public UsableHandler() {
		individualFacilityUses = new ArrayList<IndividualFacilityUse>();
	}


	@Override
	public void assignFacilityToUse(FacilityUser fU, String useDescription) throws Exception {
		if(this.isFacilityInUse())
			throw new Exception("Facility already in use. UsableHandler::assignFacilityToUse");
		this.getIndividualFacilityUses().add(new IndividualFacilityUse(fU, useDescription));
	}

	@Override
	public void vacateFacility() {
		if(this.getIndividualFacilityUses().size() == 0)
			return;
		
		this.getIndividualFacilityUses().get(this.getIndividualFacilityUses().size() - 1).endUse();	
	}

	@Override
	public boolean isFacilityInUse() {
		if(this.getIndividualFacilityUses().size() == 0)
			return false;
		
		if(this.getIndividualFacilityUses().get(this.getIndividualFacilityUses().size() - 1).getEndUseDate() == null)
			return true;
		else
			return false;
	}

	@Override
	public IndividualFacilityUse getCurrentFacilityUse() throws Exception{
		if(!this.isFacilityInUse())
			throw new Exception("Facility not in use");
		return this.getIndividualFacilityUses().get(this.getIndividualFacilityUses().size() - 1);
	}
	
	@Override
	public boolean isInUseDuringInterval() {
		//TODO
		return false;
	}


	@Override
	public ArrayList<IndividualFacilityUse> getIndividualFacilityUses() {
		return this.individualFacilityUses;
	}

	@Override
	public void setFacilityUses(ArrayList<IndividualFacilityUse> individualFacilityUses) {
		this.individualFacilityUses = individualFacilityUses;
	}



}
