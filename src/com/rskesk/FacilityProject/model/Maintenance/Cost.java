package com.rskesk.FacilityProject.model.Maintenance;

/**
 * Created by keetz on 2/9/17.
 */
public class Cost {
	
    private final double  EMP_HOURLY_RATE = 10.50;		//Hard-wired for examples sake 
 
    private double orderCost;		// total cost of order
    private int hoursOnJob;
    private double suppliesUsed;	// tape, oil, wrench things that slowly depreciate with use 
    private double suppliesPurchased;	// eg: a new shower head, a pipe joint, a door hinge, screws 


    public Cost(){
    	orderCost = -1;
        suppliesUsed=-1; 
        suppliesPurchased=-1;	
        hoursOnJob = -1;
    }
    
    public Cost(int hoursOnJob, double suppliesUsed, double suppliesPurchased){
    	this.hoursOnJob=hoursOnJob;
    	this.suppliesPurchased=suppliesPurchased;
    	this.suppliesUsed=suppliesUsed;
    	this.findOrderCost();
    	
    }
    
    public void findOrderCost(){
    	setOrderCost(suppliesPurchased+suppliesUsed+(hoursOnJob*EMP_HOURLY_RATE));
    }
   
    
	public void addNewSupplyUsed(double newSupply){
    	suppliesUsed += newSupply;
    }
    
    public void addNewSupplyPurch(double newSupply){
    	suppliesPurchased += newSupply;
    }    // GETTERS AND SETTERS

    public double getOrderCost() {
		return orderCost;
	}
	public void setOrderCost(double orderCost) {
		this.orderCost = orderCost;
	}

	public int getHoursOnJob() {
		return hoursOnJob;
	}

	public void setHoursOnJob(int hoursOnJob) {
		this.hoursOnJob = hoursOnJob;
	}
	

	public double getSuppliesUsed() {
		return suppliesUsed;
	}
	public void setSuppliesUsed(double suppliesUsed) {
		this.suppliesUsed = suppliesUsed;
	}
	
	public double getSuppliesPurchased() {
		return suppliesPurchased;
	}
	public void setSuppliesPurchased(double suppliesPurchased) {
		this.suppliesPurchased = suppliesPurchased;
	}
	public double getEmpHourlyRate(){
        return EMP_HOURLY_RATE;
    }

}