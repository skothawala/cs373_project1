package com.rskesk.FacilityProject.model.Maintenance;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;

public interface Maintainable {
	
	void makeFacilityMaintRequest(String desc, IndividualFacilityUse requestor);
	void acceptMaintenance(boolean empAssigned);
	void scheduleMaintenance();
	void completeMaintainence();
	double calcMaintenanceCostForOrder();
	double calcMaintenanceCostForFacility() ;
	double calcProblemRateForFacility();
	double calcDownTimeForFacility();
	void listMaintRequests();
	void listMaintenance();
	void listFacilityProblems();
}
