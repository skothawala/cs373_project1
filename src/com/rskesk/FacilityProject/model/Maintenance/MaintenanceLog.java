package com.rskesk.FacilityProject.model.Maintenance;

import java.time.LocalDate;

public class MaintenanceLog {
	private String description;
	private LocalDate date;
	
	public MaintenanceLog(LocalDate d, String desc){
		this.description = desc;
		this.date = d;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
}
