package com.rskesk.FacilityProject.model.Maintenance;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;

/**
 * Created by keetz on 2/10/17.
 */
public class MaintenanceOrder {
    private Cost cost;
    private MaintenanceStatus status;
    private IndividualFacilityUse requestor;
    
    public MaintenanceOrder(){
       	cost = new Cost((int) (Math.random() * 24), Math.random() * 24, Math.random() * 14);
    	status = MaintenanceStatus.PENDING;

    }

	public Cost getCost() {
		return cost;
	}

	public void setCost(Cost cost) {
		this.cost = cost;
	}

	public MaintenanceStatus getStatus() {
		return status;
	}

	public void setStatus(MaintenanceStatus status) {
		this.status = status;
	}
    
    
}