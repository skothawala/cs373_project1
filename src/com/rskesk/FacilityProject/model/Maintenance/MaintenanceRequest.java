package com.rskesk.FacilityProject.model.Maintenance;

import java.time.LocalDate;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;


/**
 * Created by keetz on 2/9/17.
 */
public class MaintenanceRequest {
	private MaintenanceOrder maintenanceOrder;
    private String description;
    private LocalDate dateOfRequest;	// Dates >> requested, scheduled, completed
    private boolean employeeAssigned;	// if true, change status to ACCEPTED
    // priority?
    
    private IndividualFacilityUse requestor;
    
    public MaintenanceRequest(String description, IndividualFacilityUse requestor){
    	this.description=description;
    	dateOfRequest=LocalDate.now();
    	maintenanceOrder=new MaintenanceOrder();
    	this.requestor = requestor;
    }
    

	public MaintenanceOrder getMaintainenceOrder() {
		return maintenanceOrder;
	}

	public void setMaintainenceOrder(MaintenanceOrder maintenanceOrder) {
		this.maintenanceOrder = maintenanceOrder;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDateOfRequest() {
		return dateOfRequest;
	}

	public void setDateOfRequest(LocalDate dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}
   
    public IndividualFacilityUse getRequestor(){
    	return this.requestor;
    } 
    

}
