package com.rskesk.FacilityProject.model.Maintenance;

public enum MaintenanceStatus {
	PENDING,
	ACCEPTED,
	SCHEDULED,
	COMPLETED
}
