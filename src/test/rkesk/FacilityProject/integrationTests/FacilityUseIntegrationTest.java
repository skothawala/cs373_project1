package test.rkesk.FacilityProject.integrationTests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.rskesk.FacilityProject.model.Facility.ApartmentBuilding;
import com.rskesk.FacilityProject.model.Facility.FacilityInformation;
import com.rskesk.FacilityProject.model.Facility.FacilityInformationHandler;
import com.rskesk.FacilityProject.model.Facility.User.FacilityUser;

public class FacilityUseIntegrationTest {

	@Test
	public void initiallyFacilityIsNotInUse() throws Exception {
		FacilityInformation apartmentfI = new FacilityInformationHandler();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		
		ApartmentBuilding apartmentBuilding = new ApartmentBuilding(apartmentfI);
		
		assertFalse(apartmentBuilding.getFacilityUses().isFacilityInUse());
	}

	@Test
	public void onceYouAssignAUserCommaFacilityIsInUse() throws Exception {
		FacilityInformation apartmentfI = new FacilityInformationHandler();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		
		ApartmentBuilding apartmentBuilding = new ApartmentBuilding(apartmentfI);
		apartmentBuilding.getFacilityUses().assignFacilityToUse(new FacilityUser("Saad", "K", ":)"), "Testing");
		
		assertTrue(apartmentBuilding.getFacilityUses().isFacilityInUse());
	}
	
	@Test
	public void onceYouVacateAFacilityCommaFacilityIsNotInUseAnymore() throws Exception {
		FacilityInformation apartmentfI = new FacilityInformationHandler();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		
		ApartmentBuilding apartmentBuilding = new ApartmentBuilding(apartmentfI);
		apartmentBuilding.getFacilityUses().assignFacilityToUse(new FacilityUser("Saad", "K", ":)"), "Testing");
		apartmentBuilding.getFacilityUses().vacateFacility();
		
		assertFalse(apartmentBuilding.getFacilityUses().isFacilityInUse());
	}
}
