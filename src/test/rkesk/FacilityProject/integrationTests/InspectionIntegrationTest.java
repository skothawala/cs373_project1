package test.rkesk.FacilityProject.integrationTests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rskesk.FacilityProject.model.Facility.ApartmentBuilding;
import com.rskesk.FacilityProject.model.Facility.FacilityInformation;
import com.rskesk.FacilityProject.model.Facility.FacilityInformationHandler;
import com.rskesk.FacilityProject.model.Facility.Inspection.Inspection;
import com.rskesk.FacilityProject.model.Facility.Inspection.InspectionHandler;

public class InspectionIntegrationTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}
	
	@Test
	public void correctlyPrintsPreviousInspections() throws Exception {
		FacilityInformation apartmentfI = new FacilityInformationHandler();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		
		ApartmentBuilding apartmentBuilding = new ApartmentBuilding(apartmentfI);
		
		apartmentBuilding.setFacilityInspection(new InspectionHandler());
		apartmentBuilding.getFacilityInspection().addInspection(new Inspection(true, LocalDate.of(2015, 4, 12)));
		apartmentBuilding.getFacilityInspection().listInspections();
		
		assertEquals("************************************\nDate of Inspection: APRIL/12/2015\nResult: true\n************************************\n",
				outContent.toString());
		
	}
	
	@Test
	public void correctlyCalculatesTheNextInspectionDate() throws Exception {
		FacilityInformation apartmentfI = new FacilityInformationHandler();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		
		ApartmentBuilding apartmentBuilding = new ApartmentBuilding(apartmentfI);
		
		apartmentBuilding.setFacilityInspection(new InspectionHandler(5));
		apartmentBuilding.getFacilityInspection().addInspection(new Inspection(true, LocalDate.of(2015, 4, 12)));
		
		
		assertEquals(LocalDate.of(2015, 4, 17), apartmentBuilding.getFacilityInspection().getNextInspectionDate());

		
	}

}
