package test.rkesk.FacilityProject.unitTests;

import static org.junit.Assert.*;
import org.junit.Test;
import com.rskesk.FacilityProject.model.Facility.User.FacilityUser;

public class FacilityUserUnitTest {

	@Test
	public void testConstructor(){
		FacilityUser guy = new FacilityUser("Guy", "Buddy", "123-456-7890");
		assertEquals(guy.getFirstName(),"Guy");
		assertEquals(guy.getLastName(),"Buddy");
		assertEquals(guy.getSsn(),"123-456-7890");
		
	}
}
